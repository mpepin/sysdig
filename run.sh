#!/bin/bash

CPU=_CPU_build/cpu.net
ASM="test/asm/$1.s"

function usage {
  echo "File $1 doesn't exist"
  echo "usage: run.sh prog"
  exit 1
}

if [ -r "$CPU" ]; then
  if [ -r "$ASM" ]; then
    simulator/simul.native -v --asm $ASM $CPU
  else
    usage $ASM
  fi
else
    usage $CPU
fi

