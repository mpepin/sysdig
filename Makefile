BUILD_DIR=_CPU_build
TEST_DIR=_test_build
SRC=$(wildcard cpu/src/*.mj)
CPU=$(BUILD_DIR)/cpu

.PHONY: simulator all minijazz fullclean test clean pre_build pdf printer

all: $(CPU).net

simulator:
	@echo "==> Building simulator"
	cd simulator && make all

minijazz:
	@echo "==> Building the minijazz compiler"
	cd minijazz && make

$(CPU).net: $(SRC) minijazz pre_build
	@echo "==> Building CPU"
	cd cpu && make
	@rm -fr $(BUILD_DIR)
	@mkdir $(BUILD_DIR)
	@mv cpu/cpu.mj $(CPU).mj
	minijazz/mjc.native -m main $(CPU).mj

pre_build:
	cd cpu && make pre_build

printer:
	ocamlbuild printer.native

run: simulator $(CPU).net printer
	@echo "==> Running CPU"
	./simulator/simul.native --asm test/asm/clock.s _CPU_build/cpu.net | ./printer.native

test: minijazz simulator pre_build
	@echo "==> Building asm tests"
	cd simulator && make asm
	@echo "==> Running asm tests"
	test/test_asm.sh
	@echo "==> Running minijazz tests"
	test/test_mj.sh

pdf:
	@echo "==> Building RAPPORT.pdf"
	cd rapport && make
	@echo "Rapport built: rapport/RAPPORT.pdf"

clean:
	rm -rf $(BUILD_DIR)
	rm -rf $(TEST_DIR)
	rm -f test/net/*_sch.net
	rm -f test/mj/*.net

fullclean: clean
	rm -f *.pdf
	rm -f RAPPORT.pdf
	cd simulator && make clean
	cd minijazz && make clean
	cd cpu && make clean
	cd rapport && make clean
	ocamlbuild -clean
