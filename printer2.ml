let input_int () =
  let line = input_line stdin in
  try int_of_string line
  with _ -> Printf.eprintf "==>%s<==\n" line ; flush stdout ; exit 2

let rec main () =
  let sec = input_int () in
  let min = input_int () in
  let h = input_int () in
  let _ = input_line stdin in
  Printf.printf "%.2d : %.2d : %.2d\n" h min sec;
  flush stdout;
  main ()

let () = main ()
