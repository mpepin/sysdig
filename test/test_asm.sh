#!/bin/bash

BIN=simulator/asm.native

function print_res {
    pad=$(printf '%0.1s' "."{1..50})
    printf '%s' "$1"
    printf '%*.*s' 0 $((50 - ${#1})) "$pad"
    printf '%s\n' "$2"
}

for file in test/asm/*.s; do
    ./$BIN $file > /dev/null;
    case $? in
        "0")
            print_res $file OK;;
        *)
            print_res $file FAIL;;
    esac
done
