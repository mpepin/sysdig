#!/bin/bash

BUILD=_test_build

pre=cpu/pre_build.native
simul=simulator/simul.native
mj=minijazz/mjc.native

function print_res {
    pad=$(printf '%0.1s' "."{1..50})
    printf '%s' "$1"
    printf '%*.*s' 0 $((50 - ${#1})) "$pad"
    printf '%s\n' "$2"
}

rm -rf $BUILD
mkdir $BUILD

function chop_suffix {
    echo "$(echo $1 | cut -f 1 -d .)"
}

function compile {
    name="$(echo $1 | cut -f 3 -d '/')"
    target="$BUILD/$name"
    $pre --sources cpu/src -o $target $1 > /dev/null
    $mj $target > /dev/null
    tmp=$?
    echo "$(chop_suffix $target).net"
    return $tmp
}
    
for file in test/cpu/*.mj; do
    # Compiling the circuit
    net_file=$(compile $file)
    if [ $? -eq 0 ]; then
      # Testing the circuit
      base=$(chop_suffix $file)
      $simul --input $base.in --output $base.out $net_file > /dev/null
      result=$?
      if [ $result -eq 0 ]; then
        print_res $file OK
      elif [ $result -eq 1 ]; then
        print_res $file FAIL
      else
        print_res $file ERROR
      fi
    else
      print_res $file ERROR
    fi
done

