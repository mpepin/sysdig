.text
# FIXME: currently only displays that which has been updated.

main:
# initialisation of the month discrimination
# 0..0101011010101
  addi $s0, $zero, 2773
# TODO: change initialisation to input value (for example, linux time stamp)
# initialisation of the value keeping the minutes and seconds
# Format : DDDD.UUUU.dddd.uuuu where d/D = digit, u/U = unit  for sec/min
  xor $s1, $s1, $s1

# initialisation of the value keeping the hours and the days
# Format : ddddd.HHHH.hhhh where d = day, h/H = hour (unit/digit)
  addi $s2, $zero, 4352

# initialisation of the value keeping the years
  addi $s3, $zero, 2017
  xor $s3, $s3, $s3

# initialisation of the value counting the leap years and the month
# Format : mmmm.ccccc where m = month, c = counter
  addi $s4, $zero, 32

# initialisation of the value keeping track of the current time
  xor $s5, $s5, $s5

# $s6 and $s7 will be used to store addresses for future jumps
# $t6 will be used to remember which values have already been printed
# $t7 will be used as a flag for when we are processing hours
  j clock

clock:
# We loop until a second has passed.
# Comment this part for performance tests.
#  li $v0, 5
#  syscall
#  beq $v0, $s5, clock
# We update the time.
#  add $s5, $zero, $v0
# We empty the buffer
  la $a0, end
  li $v0, 4
  syscall
# We now start the calculations for the next second.
  j seconds

print_zero:
  la $a0, zero
  addi $t0, $zero, 1
  xor $a1, $a1, $a1
  j print

print_zero_digit:
  la $a0, zero
  addi $t0, $zero, 1
  xor $a2, $a2, $a2
  j print

print_one:
  la $a0, one
  j print

print_two:
  la $a0, two
  j print


print_three:
  la $a0, three
  j print

print_four:
# HACK : $t7 = 2 when dealing with hours -> check if digit = 2
# We test whether this is the 4 of 24 hours. If so, jump to twenty_four
  and $t2, $t7, $a2
  bne $t2, $zero, print_twenty_four
  la $a0, four
  j print

# We handle the case of 24h, coming from the units.
print_twenty_four:
  xor $a1, $a1, $a1
  addi $t0, $zero, 1
  la $a0, zero
  j print

print_five:
  la $a0, five
  j print

print_six:
  la $a0, six
  j print

print_seven:
  la $a0, seven
  j print

print_eight:
  la $a0, eight
  j print

print_nine:
  la $a0, nine
  j print

print:
  li $v0, 4
  syscall
  jr $ra

# To make a conditional jump to $ra
break:
  jr $ra

# We use this block to compute the display of a unit - seconds, minutes or hours
unit:
  xor $t0, $t0, $t0
  xor $v0, $v0, $v0
# $t0 is a flag at 1 iff an update is needed
  addi $t1, $zero, 1
# We increment $a1
  addi $a1, $a1, 1
# We use $t0 as an easy way to access 0, but here $t0 = $zero
# We set the return adress to be the digit
  la $ra, digits
# We test the value of $a1
  beq $a1, $t1, print_one
  addi $t1, $t1, 1
  beq $a1, $t1, print_two
  addi $t1, $t1, 1
  beq $a1, $t1, print_three
  addi $t1, $t1, 1
  beq $a1, $t1, print_four
  addi $t1, $t1, 1
  beq $a1, $t1, print_five
  addi $t1, $t1, 1
  beq $a1, $t1, print_six
  addi $t1, $t1, 1
  beq $a1, $t1, print_seven
  addi $t1, $t1, 1
  beq $a1, $t1, print_eight
  addi $t1, $t1, 1
  beq $a1, $t1, print_nine
  addi $t1, $t1, 1
  beq $a1, $t1, print_zero

# We use this block to compute the display of a digit - seconds, minutes, hours
digits:
# We set the return value
  add $ra, $zero, $s7
# We test if an update is necessary
  beq $t0, $zero, break
  add $ra, $zero, $s6
# We test whether we reached 24 hours
# Remember that $t2 = 2 when we deal with hours.
  bne $t2, $zero, print_zero_digit
  xor $t0, $t0, $t0
  addi $a2, $a2, 1
  addi $t1, $zero, 1
# We test the value of $a2
  beq $a2, $t1, print_one
  addi $t1, $t1, 1
  beq $a2, $t1, print_two
  addi $t1, $t1, 1
  beq $a2, $t1, print_three
  addi $t1, $t1, 1
  beq $a2, $t1, print_four
  addi $t1, $t1, 1
  beq $a2, $t1, print_five
  addi $t1, $t1, 1
  beq $a2, $t1, print_zero_digit

# The following blocks compute a possible update of the month.
# This is where we compute whether it is a leap year or not.
day_29:
# We are sent here from the tests on the day
# $a1 is the day, $a2 is the month
  addi $t4, $zero, 2
  bne $a2, $t4, update_days
# We test whether the year is a leap year
# First, if the modulo is 4
  andi $t4, $s3, 3
  beq $t4, $zero, year_100
  addi $t0, $zero, 1
  j update_days

# Then, if it is a multiple of four, whether our leap year counter is 25
year_100:
# Update the counter first.
  addi $s4, $s4, 1
  addi $t4, $zero, 25
  beq $s4, $t4, year_400

# If we didn't jump, it's a leap year.
leap_year:
# The day should be 1
  addi $a1, $zero, 1
  addi $t0, $zero, 1
  j update_days

year_400:
# We need to reset our counter
  xor $s4, $s4, $s4
# We test whether the year is a multiple of 400
# If it is, it's a leap year
  andi $t4, $a1, 15
  beq $t4, $zero, leap_year
  addi $a1, $a1, 1
  j update_days

# This is to test whether we are a 29/02
day_30:
  addi $t4, $zero, 2
  bne $a2, $t4, update_days
# If this is indeed a 29/02
  addi $a1, $zero, 1
  addi $t0, $zero, 1
  j update_days

day_31:
# We use the discrimination from $s0 to decide if a month has 30 or 31 days
  srlv $t4, $s0, $a2
  andi $t4, $t4, 1
# $t4 = 0 if the month has 30 days, $t4 = 1 otherwise
  bne $t4, $zero, update_days
# j day_32

# Here, we always need to update the month.
day_32:
  addi $a1, $zero, 1
  addi $t0, $zero, 1
  j update_days

# For when we need to roll around to a new year
month_13:
  addi $a2, $zero, 1
  addi $t0, $zero, 1
  j update_months


# Beginning of each loop
seconds:
  xor $t7, $t7, $t7
  xor $a1, $a1, $a1
  xor $a2, $a2, $a2
# We fetch the stored values of the seconds
  andi $a1, $s1, 15
  andi $a2, $s1, 240
  srl $a2, $a2, 4
# We empty the print argument
  xor $a0, $a0, $a0
  xor $t0, $t0, $t0
# $t2 is the flag for when we reached 24h.
  xor $t2, $t2, $t2
  la $s7, update_sec_units
  la $s6, update_sec_digits
  j unit

update_sec_digits:
# Update the digits
  andi $s1, $s1, 65295
  sll $a2, $a2, 4
  or $s1, $a2, $s1

update_sec_units:
# Update the units
  andi $s1, $s1, 65520
  or $s1, $a1, $s1
  beq $t0, $zero, clock
#  j minutes

minutes:
  xor $a0, $a0, $a0
  xor $a1, $a1, $a1
  xor $a2, $a2, $a2
  xor $t0, $t0, $t0
# We fetch the stored values of the minutes
  andi $a1, $s1, 3840
  srl $a1, $a1, 8
  andi $a2, $s1, 61440
  srl $a2, $a2, 12
  la $s7, update_min_units
  la $s6, update_min_digits
  j unit

update_min_digits:
# Update the digits
  andi $s1, $s1, 4095
  sll $a2, $a2, 12
  or $s1, $a2, $s1

update_min_units:
# Update the units
  andi $s1, $s1, 61695
  sll $a1, $a1, 8
  or $s1, $a1, $s1
  beq $t0, $zero, clock
#  j hours

hours:
  xor $a0, $a0, $a0
  xor $a1, $a1, $a1
  xor $a2, $a2, $a2
  xor $t0, $t0, $t0
# We are treating the hours, so $t7 = 2
  addi $t7, $zero, 2
# We fetch the stored values of the hours
  andi $a1, $s2, 15
  andi $a2, $s2, 240
  srl $a2, $a2, 4
# We set the return adresses
  la $s7, update_hours_units
  la $s6, update_hours_digits
  j unit

update_hours_digits:
# Update the digits
  andi $s2, $s2, 65295
  sll $a2, $a2, 4
  or $s2, $a2, $s2

update_hours_units:
# Update the units
  andi $s2, $s2, 65520
  or $s2, $a1, $s2
  beq $t0, $zero, clock
#  j days

days:
  xor $a0, $a0, $a0
  xor $a1, $a1, $a1
  xor $a2, $a2, $a2
  xor $t0, $t0, $t0
# We fetch the current day and month
  andi $a1, $s2, 7936
  srl $a1, $a1, 8
  andi $a2, $s4, 480
  srl $a2, $a2, 5
# We update the day
  addi $a1, $a1, 1
# We have some tests to do according to the next day
  addi $t3, $zero, 29
  beq $a1, $t3, day_29
  addi $t3, $t3, 1
  beq $a1, $t3, day_30
  addi $t3, $t3, 1
  beq $a1, $t3, day_31
  addi $t3, $t3, 1
  beq $a1, $t3, day_32
# If we haven't jumped, we must stay in the same month

update_days:
# We print the new day
  add $a0, $zero, $a1
  li $v0, 1
  syscall
  la $a0, line
  li $v0, 4
  syscall
# Screen $s2 to erase the days
  andi $s2, $s2, 255
  sll $a1, $a1, 8
  or $s2, $a1, $s2
  beq $t0, $zero, clock
# j months

months:
  xor $t0, $t0, $t0
# At this point, $a2 already contains the month
  addi $a2, $a2, 1
  addi $t4, $zero, 13
  beq $a2, $t4, month_13
# j update_months

update_months:
# We print the new month
  add $a0, $zero, $a2
  li $v0, 1
  syscall
  la $a0, line
  li $v0, 4
  syscall
# Screen $s3 to erase the month
  andi $s4, $s4, 31
  sll $a2, $a2, 5
  or $s4, $s4, $a2
  beq $t0, $zero, clock
# j years

years:
# We only have to add 1 to the current year.
  addi $s3, $s3, 1
# We print the new year
  add $a0, $zero, $s3
  li $v0, 1
  syscall
  la $a0, line
  li $v0, 4
  syscall
# Time to restart the loop
  j clock

.data
  zero: .asciiz "1011111\n"
  one: .asciiz "0000110\n"
  two: .asciiz "0111011\n"
  three: .asciiz "0101111\n"
  four: .asciiz "1100110\n"
  five: .asciiz "1101101\n"
  six: .asciiz "1111101\n"
  seven: .asciiz "0000111\n"
  eight: .asciiz "1111111\n"
  nine: .asciiz "1101111\n"
  line: .asciiz "\n"
  end: .asciiz "EOF\n"
