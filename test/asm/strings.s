.text
main:
  la $a0, hello
  li $v0, 1
  syscall

  la $a0, world
  li $v0, 1
  syscall

  la $a0, hello
  li $v0, 1
  syscall

  la $a0, hello
  addi $a0, 8
  li $v0, 1
  syscall

end:
  li $v0, 10
  syscall

.data
  hello: .asciiz "Hello"
  world: .asciiz "World\n"
