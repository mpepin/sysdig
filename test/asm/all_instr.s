.text
	# Segment du programme

	# Toutes les instructions normalement supportées par notre processeur MIPS-lite
	# Elles apparaissent dans l'ordre utilisé dans le document Google Sheet qui les répertorie
	add 	$zero, $zero, $zero
	addi 	$zero, $zero, 65535
	sub 	$zero, $zero, $zero
	mult 	$zero, $zero
	div 	$zero, $zero

	or 		$zero, $zero, $zero
	ori 	$zero, $zero, 65535
	and 	$zero, $zero, $zero
	andi 	$zero, $zero, 65535
	xor 	$zero, $zero, $zero
	xori 	$zero, $zero, 65535
#	nor 	$zero, $zero, $zero

	slt 	$zero, $zero, $zero
	slti 	$zero, $zero, 65535

	sllv 	$zero, $zero, $zero
	sll 	$zero, $zero, 31
	srlv 	$zero, $zero, $zero
	srl 	$zero, $zero, 31
	srav 	$zero, $zero, $zero
	sra 	$zero, $zero, 31

	lw 		$zero, 65535($zero)
	lh 		$zero, 65535($zero)
	lb 		$zero, 65535($zero)
	sw 		$zero, 65535($zero)
	sh 		$zero, 65535($zero)
	sb 		$zero, 65535($zero)
#	lui 	(pseudo instruction)
	mfhi 	$zero
	mflo 	$zero

	beq 	$zero, $zero, jump_to_bne
jump_to_bne:
	bne 	$zero, $zero, jump_to_j
jump_to_j:
	j 		jump_to_jr
jump_to_jr:
	jr 		$zero
	jal 	jump_to_nop

jump_to_nop:
	nop 	
	syscall 

.data
	# Segment de données
