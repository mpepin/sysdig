.text
main:
	# Suite de sauts, qui doit afficher 0, 1, 2 et 3 dans l'ordre
	li $a0, 0
	jal print_int

	# Affiche 1
	jal print_1

	# Affiche 2
	la $ra, here
	j print_2
here:
	# Affiche 3 et quitte
	la $t0, print_3_and_exit
	jr $t0

	# Ne devrait pas arriver !
	li $a0, 999999 
	syscall

print_1:
	li $a0, 1
	add $t1, $zero, $ra
	jal print_int
	add $ra, $zero, $t1
	jr $ra

	# Ne devrait pas arriver !
	li $a0, 999999 
	syscall

print_2:
	li $a0, 2
	add $t1, $zero, $ra
	jal print_int
	add $ra, $zero, $t1
	jr $ra

	# Ne devrait pas arriver !
	li $a0, 999999 
	syscall

print_3_and_exit:
	li $a0, 3
	add $t1, $zero, $ra
	jal print_int
	add $ra, $zero, $t1
	j exit

	# Ne devrait pas arriver !
	li $a0, 999999 
	syscall

print_int:
	# Attends un entier dans $a0
	li $v0, 1
	syscall
	jr $ra

	# Ne devrait pas arriver !
	li $a0, 999999 
	syscall

exit:
	# Fin du programme
	li $v0, 10
	syscall

	# Ne devrait pas arriver !
	li $a0, 999999 
	syscall

.data
