.text
main:
    addi $t0, $zero,100	# Nombre d'itérations de la somme
    xor  $a0, $a0, $a0 	# Mise à zéro de l'accumulateur
f:
    beq  $zero, $t0, end
    add  $a0, $a0, $t0 	# $a0 = $a0 + $t0
    addi $t0, $t0, -1	# ($t0)--
    j    f
end:
    li $v0, 1 			# Appel système pour afficher le résultat ($a0)
    syscall 
exit:
    li $v0, 10
    syscall
.data
