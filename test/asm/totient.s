.text
main:
	la $a0, query
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	add $t0, $v0, $zero
	addi $t1, $t0, -1
	li $t2, 0 
	
tot:
	slt $at, $zero, $t1
	beq $at, $zero, done
	add $a0, $t0, $zero
	add $a1, $t1, $zero
	jal gcd
	addi $t3, $v0, -1
	beq $t3, $zero, inc
	addi $t1, $t1, -1
	beq $zero, $zero, tot 
	
inc:
	addi $t2, $t2, 1
	addi $t1, $t1, -1
	beq $zero, $zero, tot

gcd:
	addi $sp, $sp, -12
        sw $a1, 8($sp)
 	sw $a0, 4($sp)
        sw $ra, 0($sp)
 	add $v0, $a0, $zero				
	beq $a1, $zero, gcd_return
	add $t4, $a0, $zero
	add $a0, $a1, $zero
	div $t4, $a1
	mfhi $a1
 	jal gcd
	lw $a1, 8($sp)
	lw $a0, 4($sp)

gcd_return:
	lw $ra, 0($sp)
	addi $sp, $sp, 12
	jr $ra

done:

	la $a0, result_msg
	li $v0, 4
	syscall

	add $a0, $t2, $zero
	li $v0, 1
	syscall

	li $v0, 10
	syscall
.data
  query: .asciiz "Input m = "
  result_msg: .asciiz "Totient(m) = "	
