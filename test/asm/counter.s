.text
main:
	# Secondes : $s0 ($t1 = 60)
	# Minutes  : $s1 ($t1 = 60)
	# Heures   : $s2 ($t2 = 24)

	# Initialisation
	xor $s0, $s0, $s0
	xor $s1, $s1, $s1
	xor $s2, $s2, $s2

	addi $t1, $zero, 60
	addi $t2, $zero, 24

	la $t8, newline
	la $t9, vspace

	# Exemple : 57 secondes, 42 minutes
	addi $s0, $zero, 57
	addi $s1, $zero, 42

start:
	# Secondes + 1
	addi $s0, $s0, 1
	bne $s0, $t1, print_and_loop

	# Secondes = 0, minutes + 1
	addi $s0, $zero, 0
	addi $s1, $s1, 1
	bne $s1, $t1, print_and_loop

	# Minutes = 0, heures + 1
	xor $s1, $s1, $s1
	addi $s2, $s2, 1
	bne $s2, $t2, print_and_loop

	# Heures = 0, jour + 1
	xor $s2, $s2, $s2
	addi $s3, $s3, 1
	bne $s3, $t3, print_and_loop

print_and_loop:
	add $a0, $zero, $s0
	li $v0, 1
	syscall
	li $v0, 4
	add $a0, $zero, $t8
	syscall

	add $a0, $zero, $s1
	li $v0, 1
	syscall
	li $v0, 4
	add $a0, $zero, $t8
	syscall

	add $a0, $zero, $s2
	li $v0, 1
	syscall
	li $v0, 4
	add $a0, $zero, $t9
	syscall

	# sync
	j start

.data
	newline: .asciiz "\n"
	vspace: .asciiz "\n\n"
