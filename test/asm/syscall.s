.text
main:
    addi $a0, $zero, 42
    addi $v0,$zero,1
    syscall

    la $a0, hello
    addi $v0,$zero, 4
    syscall

    addi $v0, $zero, 10
    syscall
.data
  hello: .asciiz "\nHello, world\n"
