\documentclass[11pt]{article}
\usepackage{lmodern}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{url}
\usepackage[margin=2.5cm, paper=a4paper]{geometry}
\usepackage{cite}

\title{Systèmes numériques --- Rapport final}
\author{Martin Pépin, Camille Gobert, Noémie Fong, Anatole Dahan}

% \date{dimanche 11 décembre 2016}

\begin{document}

\maketitle

\emph{Note :} spécifie les choix techniques qui concernent le projet et le simulateur. Voir le fichier \texttt{README.md} pour le mode
d'emploi et l'organisation du code.

\section{Introduction}
  Nous avons décidé de fortement nous inspirer de l'architecture MIPS, dans le
  but de réaliser un micro-processeur à visée relativement généraliste, et non
  spécifiquement dédié à une utilisation en tant que montre numérique.

  L'architecture générale (registres, jeu d'instructions, décodage, lectures et
  écritures...) s'appuie donc sur ce que MIPS propose. Cependant, pour des
  raisons de simplicité, notre objectif premier était de réaliser quelque-chose
  que nous comprenons bien, et qui fonctionne. Aussi, nous avons choisi un jeu
  d'instruction limité, ignoré certains modules (tels que le co-processeur dédié
  aux nombres flottants). L'implémentation d'une logique de \textit{pipeline}
  n'était pas prévue pour les mêmes raisons.

  Outre le fait de nous appuyer sur des ressources existantes et sur une
  architecture --- et ses choix --- déjà éprouvée, nous avons aussi opté pour
  MIPS avec l'idée de pouvoir réutiliser des outils existants. Nous n'avons pas eu
  le temps de réaliser cela, ni même tout ce que nous avions prévu, mais nous
  souhaitions pouvoir exécuter des programmes simples conçus à partir
  d'un langage haut niveau, tel que C. Celui-ci aurait pu être compilé et
  optimisé par des outils puissants qui existent déjà (\texttt{gcc} ou
  \texttt{clang}, par exemple) --- en prenant bien entendu soin de ne pas
  requérir aux bibliothèques standards, ou à des éléments délibérément non
  implémentés dans ce projet.

  Ci-après, vous pourrez trouver des informations concernant différentes parties
  du projet que nous avons réalisées.

\section{Simulateur}

  \subsection{Structure du simulateur}
    
    Chaque cycle se découpe en plusieurs étapes.

    \begin{itemize}
      \item On parcours et analyse d'abord les entrées de l'utilisateur à l'aide de
        \texttt{Scanf} et on vérifie au passage que les entrées sont bien
        formées.
      \item On évalue les instructions.
      \item On met à jour les registres.
      \item On oublie les variables intermédiaires (cf~\ref{simulator-vars})
      \item On traite les appels systèmes en inspectant le contenu de la RAM,
        voir~\ref{syscalls}.
      \item On traite les~\texttt{sync} en mettant éventuellement à jour le
        contenu de la RAM à~$\texttt{0x00000001}$.
    \end{itemize}

  \subsection{La mémoire}

    Tout ce qui est relatif à la mémoire est encapsulé dans le module
    \texttt{Interpreter.Env}. Cela nous a permis de faire plusieurs changements
    d'implémentation sans trop d'efforts au cours de l'avancée du projet.
    L'implémentation finale est la suivante : 

    \subsubsection{RAM et ROM}

      La RAM et la ROM sont des matrices dont les lignes sont les octets stockés
      en mémoire. Lors de chaque accès, la taille de l'adresses mémoire fournie
      est vérifiée puis transformées en entier (conversion binaire--décimal).
      On leur donne la taille~$4096$ par défaut. Si la place vient à manquer
      non peut ajouter un argument optionnel au simulateur.

      La gestion de la RAM (et de la ROM qui est similaire) est aussi encapsulée
      dans un module à part ; voir \texttt{simulator/ram.ml} et
      \texttt{simulator/ram.mli}.

    \subsubsection{Variables}\label{simulator-vars}

      Les variables sont stockées dans une table de hachage, facile à gérer tout
      le long du programme car mutable. À la fin de chaque tour de boucle, cette
      table est vidée.

    \subsubsection{Registres}

      Les registres sont également représentés par une table de hachage, et ils sont
      mis à jour à la fin de chaque tour de boucle, avant la remise à zéro des
      variables. Au premier tour de boucle, cette table est vide. On considère
      que tous les registres sont à \texttt{false} et on profite de ce premier
      passage pour remplir la table des registres

\section{Architecture générale du micro-processeur}

Étant donné que nous nous inspirons fortement de MIPS, c'est également vrai
concernant l'organisation générale du micro-processeur. Les étapes de décodage
des instructions, de lecture des registres et de la mémoire, de calculs et
d'écriture des registres et de la mémoire, et de la mise à jour du pointeur
d'instruction (\textit{Program Counter}) sont ainsi réalisées de façon très
similaire à celle de MIPS. A défaut de décrire précisément chacune de ces
étapes, nous pensons nous appuyer sur deux références :

  \begin{enumerate}
    \item D'une part, le cours ayant été donné à ce sujet (le 29/11/2016), nous
      a fourni une idée relativement précise de la structure d'un
      micro-processeur MIPS.
    \item D'autre part, pour des schémas et des explications détaillés, nous
      souhaitons utiliser le livre \textit{Computer Organization and Design}
      \cite{patterson2013computer}. Un schéma de l'architecture MIPS proche de
      ce que nous souhaitons implémenter est donné dans la figure
      \ref{ex-architecture-mips}.
  \end{enumerate}

  \begin{figure}[h!]
    \includegraphics[width=\textwidth]{architecture-mips.png}
    \caption{Exemple d'un schéma de l'architecture MIPS proche de ce que nous
    souhaitons réaliser. Ce schéma provient du livre que nous mentionnons,
    également cité comme référence dans le cours \cite{patterson2013computer}.}
    \label{ex-architecture-mips}
  \end{figure}

  \subsection*{Jeu de registres}
    Tous les registres seront concentrées dans une seule unité, appelée
    \textit{banc de registres}. Il contient les entrées et sorties spécifiées
    sur le schéma donné en exemple (figure \ref{ex-architecture-mips}).

    Les registres spéciaux \texttt{LO} et \texttt{HI} ne sont pas directement
    accessibles (il faut passer par des instructions dédiées), mais le
    micro-processeur peut y lire et y écrire des valeurs. Dans notre implémentation,
    ils se trouvent en réalité dans l'UAL.

  \begin{center}
    \begin{tabular}{|c|c|l|}
      \hline
      \textbf{Registre} & \textbf{Numéro} & \textbf{Conventions associées} \\
      \hline
      \hline
      \texttt{\$zero} & 0 & Non-modifiable ; toujours égal à zéro \\ 
      \texttt{\$at} & 1 & Réservé à l'assembleur\\
      \texttt{\$v0},\texttt{\$v1} & 2,3 & Valeurs de retour des fonctions\\
      \texttt{\$a0}\dots\texttt{\$a3} & 4\dots 7 & Arguments des fonctions \\
      \texttt{\$t0}\dots\texttt{\$t7} & 8\dots 15 & Registres temporaires
      (non sauvegardés lors d'un appel) \\
      \texttt{\$s0}\dots\texttt{\$s7} & 16\dots 23 & Registres sauvegardés
      (à sauvegarder lors des appels)\\
      \texttt{\$t8},\texttt{\$t9} & 24,25 & Registres temporaires,
      (non sauvegardés lors d'un appel) \\
      \texttt{\$k0},\texttt{\$k1} & 26,27 & Réservés à l'OS (dans ce projet, ils
      sont donc libres) \\
      \texttt{\$gp} & 28 & Pointeur vers le contexte global (i.e. segment
      \texttt{.data} en MIPS) \\
      \texttt{\$sp} & 29 & Pointeur vers le sommet de la pile\\
      \texttt{\$fp} & 30 & Pointeur vers la début du contexte courant\\
      \texttt{\$ra} & 31 & Adresse à laquelle retourner à la fin d'un appel de fonction\\
      \hline
      \texttt{LO} & --- & Lecture uniquement indirecte ; non-modifiable par une
      instruction \\
      \texttt{HI} & --- & Lecture uniquement indirecte ; non-modifiable par une
      instruction \\ 
      \hline
    \end{tabular}
  \end{center}

  \subsection{Stockage et lecture des instructions}
    Concernant le chargement du programme et sa lecture par le micro-processeur,
    comme nous l'avons mentionné dans l'introduction, cela se fait via la
    mémoire ROM. Le programme est donc entièrement chargé dans la ROM du
    simulateur lorsque l'exécution débute. Par convention, la ROM sera formée de
    mots mémoire de taille 32 bits ; et la première instruction se trouvera à
    l'adresse \texttt{0}. Au début de chaque cycle, l'instruction à décoder sera
    lue à l'adresse égale à celle du pointeur d'instruction.

  \subsection{Unité Arithmétique et Logique (UAL)}
    Diverses opérations sont effectuées par l'unité arithmétique et logique du
    micro-processeur ; celle-ci reçoit soit deux registres, soit un registre et
    une constante (16 bits ou 5 bits), encodée dans l'instruction en entrée --- 
    ainsi que l'opération à effectuer (encodée sur 4 bits). Le choix de la seconde
    entrée de l'UAL est effectué par des multiplexeurs, contrôlés par des
    drapeaux de contrôle, levés ou non en fonction de l'instruction courante.

    Le résultat produit peut être ignoré, ou bien écrit dans un registre.
    L'UAL produit également un drapeau qui indique si la sortie calculée est égale
    à zéro ou non (il peut être utilisé en tant que drapeau de contrôle par le mciro-processeur).

    Les détails concernant les opérations que peut réaliser l'UAL sont donnés
    dans la liste des instructions, et sont expliquées dans le code source.

  \subsection{Mémoire RAM}

    La RAM est un tableau d'octets : il est possible d'y écrire et d'y lire des mots
    (4 octets, uniquement aux adresses multiples de $4$). Ceci se fait
    respectivement à l'aide des instructions \texttt{sw} et \texttt{lw}, décrites
    plus bas.

    Les lectures et écritures de demi-mots (\texttt{lh}, \texttt{sh}) et d'octet
    (\texttt{lb}, \texttt{sb})n'ont finalement pas été implémentées,
    par manque de temps et d'utilité. Cependant, une partie de notre micro-processeur
    est réalisée de sorte à ce qu'elles puissent être facilement ajoutés (par exemple,
    en généralisant certains drapeaux de contrôle à toutes les opérations de lecture ou
    d'écriture mémoire).

    Nous avons par ailleurs choisi de stocker les nombres avec la convention grand-boutiste.

  \subsection{Pointeur d'instruction}

    Le \emph{Program Counter} est un registre spécial, qui contient l'adresse de la prochaine instruction à lire par le processeur (en ROM).

    Lors d'un saut ou branchement, la valeur de ce registre est simplement
    modifiée pour reprendre l'exécution du programme à partir du point voulu :
    par la valeur contenue dans un registre (\texttt{jr}), ou par une adresse
    encodée dans une instruction de saut (\texttt{j}, \texttt{jal}) ou de branchement
    (\texttt{beq}, \texttt{bne}).

    En l'absence de branchement, un incrément de $4$ à lieu par défaut, à la fin
    d'une instruction.

  \subsection{Unités de contrôle}

    Notre micro-processeur contient deux unités de contrôle, qui régissent son
    fonctionnent à chaque cycle, et ce en fonction de l'instruction qui a été lue.

    Le premier module de contrôle est le plus général : à partir de deux parties
    de l'instruction courante, qui décrivent l'opération à effectuer, il lève ou
    non une dizaine de drapeaux. Ceux-ci servent principalement à contrôler des
    multiplexeurs à de nombreux endroits du micro-processeur, afin de faire varier
    les entrées de certains modules, d'ignorer ou non certaines, valeurs, etc.
    Par exemple, on dispose de drapeaux qui indique si on a ou saut ou non, s'il faut
    écrire une valeur dans un registre ou non, si on accède à la RAM ou non...

    Le second module de contrôle est spécifique à l'UAL : en effet, il décide de
    l'opération que doit effectuer l'UAL. Suivant le format de l'instruction, cela
    peut soit directement être encodé dans cette dernière (segment \texttt{funct}
    d'une instruction au format R), soit dicté par l'unité de contrôle principale.

    Ces modules ont été conçus de façon à supporter certaines instructions qui ne sont
    pas implémentées matériellement, en vue d'une possible extension de notre travail
    actuel.

\section{Jeu d'instructions}

  \subsection{Choix des instructions à implémenter}
    Afin de choisir un sous-ensemble cohérent d'un jeu d'instruction d'une
    version 32 bits de MIPS, nous nous sommes fortement inspiré des instructions
    de base disponibles sur la page Wikipédia associée, en anglais
    \cite{wiki-mips}. Nous le décrivons dans cette section.

    Les détails (format, encodage, drapeaux associés...) concernant le format du
    code machine associé à chaque instruction est disponible dans les
    références. Nous nous contentons donc de donner le \textbf{nom} de
    l'instruction, et une brève \textbf{description}.

    Nous n'avons pas eu le temps de réaliser toutes les instructions listées
    ci-dessous. En particulier, la multiplication, la division, et certaines opérations
    de lecture/écriture en RAM, ne sont pas disponibles. Cependant, le
    micro-processeur a été conçu en les prenant en compte : bien qu'ils manquent
    à notre UAL, les drapeaux de contrôle qui les concernent prennent en compte
    les instructions qui effectuent ces opérations (afin de faciliter un ajout futur).

    Bien qu'il existe déjà des assembleurs pour MIPS, nous avons écrit notre propre
    assembleur, et l'avons utilisé pour tous les programmes de test de ce projet
    (voir \texttt{simulator/asm/}).

  \subsection{Opérations arithmétique, bit-à-bit, logiques, et décalages}

    \begin{center}
      \begin{tabular}{|r|c|p{0.41\textwidth}|}
        \hline

        \textbf{Nom} & \textbf{Instruction MIPS} & \textbf{Description} \\

        \hline
        \hline

        Addition & \texttt{add \$x, \$y, \$z} & \texttt{\$x = \$y + \$z} \\

        Addition imm. & \texttt{addi \$x, \$y, C} & \texttt{\$x = \$y + $C$}
        ($C$ une constante 16 bits) \\

        Soustraction & \texttt{sub \$x, \$y, \$z} & \texttt{\$x = \$y - \$z} \\

        Multiplication & \texttt{mult \$x, \$y} & \texttt{HI =} 32 bits de poids
        fort de \texttt{\$y * \$z} \newline \texttt{LO =} 32 bits de poids
        faible de \texttt{\$y * \$z} \\

        Division & \texttt{div \$x, \$y} & \texttt{HI =} quotient de
        \texttt{\$y / \$z} \newline \texttt{LO =} reste de \texttt{\$y / \$z} \\

        \hline

        OU bit-à-bit & \texttt{or \$x, \$y, \$z} & \texttt{\$x = \$y | \$z} \\

        OU bit-à-bit imm. & \texttt{ori \$x, \$y, C} & \texttt{\$x = \$y | $C$}
        ($C$ une constante 16 bits)\\

        ET bit-à-bit & \texttt{and \$x, \$y, \$z} & \texttt{\$x = \$y \& \$z} \\

        ET bit-à-bit imm. & \texttt{andi \$x, \$y, C} & 
        \texttt{\$x = \$y \& $C$} ($C$ une constante 16 bits)\\

        XOR bit-à-bit & \texttt{xor \$x, \$y, \$z} &
        \texttt{\$x = \$y \string^\$z} \\

        XOR bit-à-bit imm. & \texttt{xori \$x, \$y, C} &
        \texttt{\$x = \$y \string^ $C$} ($C$ une constante 16 bits)\\

        \hline

        Comparaison & \texttt{slt \$x, \$y, \$z} & \texttt{\$x = (\$y < \$z)} \\

        Comparaison imm. & \texttt{slti \$x, \$y, C} &
        \texttt{\$x = (\$y < $C$)} ($C$ une constante 16 bits) \\

        \hline

        Déc. logique à gauche & \texttt{sllv \$x, \$y, \$z} &
        \texttt{\$x = \$y << \$z} \\

        Déc. logique à gauche imm. & \texttt{sll \$x, \$y, S} &
        \texttt{\$x = \$y << $S$} ($S$ une constante 5 bits)\\

        Déc. logique à droite & \texttt{srlv \$x, \$y, \$z} &
        \texttt{\$x = \$y >> \$z} \\

        Déc. logique à droite imm. & \texttt{srl \$x, \$y, S} &
        \texttt{\$x = \$y >> $S$} ($S$ une constante 5 bits)\\

        Déc. arith. à droite & \texttt{srav \$x, \$y, \$z} &
        \texttt{\$x = \$y >> \$z} \newline Extension de bits avec le bit de
        signe de \texttt{\$y}\\

        Déc. arith. à droite imm. & \texttt{sra \$x, \$y, S} &
        \texttt{\$x = \$y >> $S$} ($S$ une constante 5 bits) \newline Extension
        de bits avec le bit de signe de \texttt{\$y}\\

        \hline
      \end{tabular}
    \end{center}

  \subsection{Accès à la RAM et aux registres}

    \begin{center}
      \begin{tabular}{|r|c|p{0.41\textwidth}|}

        \hline

        \textbf{Nom} & \textbf{Instruction MIPS} & \textbf{Description} \\

        \hline\hline

        Charger mot & \texttt{lw \$t,C(\$s)} & \texttt{\$t = Memory[\$s + C]}
        Charge le mot stocké à l'adresse \texttt{MEM[\$s+C]} et aux 3
        octets suivants\\ 

        Charger demi-mot & \texttt{lh \$t,C(\$s)} &
	\texttt{\$t = Memory[\$s + C]} Charge le demi-mot à l'adresse spécifiée
	ainsi qu'à l'octet suivant\\

        Charger octet & \texttt{lb \$t,C(\$s)} & \texttt{\$t = Memory[\$s + C]}
        Charge l'octet à l'adresse spécifiée\\

        \hline

        Stocker mot & \texttt{sw \$t,C(\$s)} & \texttt{Memory[\$s + C] = \$t}
        Stocke le mot à l'adresse spécifiée et aux 3 octets suivants\\

        Stocker demi-mot & \texttt{sh \$t,C(\$s)} &
	\texttt{Memory[\$s + C] = \$t} Stocke les plus petits 16 bits du
	registre à l'adresse spécifiée et à l'octet suivant\\

        Stocker octet & \texttt{sb \$t,C(\$s}) & \texttt{Memory[\$s + C] = \$t}
        Stocke les plus petits 8 bits du registre à l'adresse spécifiée\\

        Charger partie sup. imm. & \texttt{lui \$t,C} &
        \texttt{\$t = C << 16} Charge une opérande immédiate de 16 bits dans les
        16 bits supérieurs du registre $t$\\

        Charger HI & \texttt{mfhi \$d} & \texttt{\$d = HI} ($d$ est un registre
        et prend la valeur de $HI$)\\

        Charger LO & \texttt{mflo \$d} & \texttt{\$d = LO} ($d$ est un registre
        et prend la valeur de $LO$)\\

        \hline
      \end{tabular}
    \end{center}

  \subsection{Sauts et branchements}
    \begin{center}
      \begin{tabular}{|r|c|p{0.42\textwidth}|}
        \hline

        \textbf{Nom} & \textbf{Instruction MIPS} & \textbf{Description} \\

        \hline\hline

        Branchement sur égalité & \texttt{beq \$s,\$t,C} &
        Si \texttt{\$s == \$t} alors \texttt{PC + 4 + 4*C}\\

        Branchement sur différence & \texttt{bne \$s,\$t,C} &
        Si \texttt{\$s != \$t} alors \texttt{PC + 4 + 4*C}\\

        \hline

        Saut & \texttt{j C} & \texttt{PC = PC+4[31:28] . C*4}\\

        Saut registre & \texttt{jr \$s} & \texttt{PC = \$s}\\

        Saut et lien & \texttt{jal C} &
        \texttt{\$ra = PC + 4} adresse de retour;
        \texttt{PC = PC + 4[31:28] . C*4} \\

        \hline
      \end{tabular}
    \end{center}

  \subsection{Divers}

    \begin{center}
      \begin{tabular}{|r|c|p{0.43\textwidth}|}
        \hline

        \textbf{Nom} & \textbf{Instruction MIPS} & \textbf{Description} \\

        \hline\hline

        Instruction vide & \texttt{nop} & Aucun effet\\

        \hline

        Appel système & \texttt{syscall} & Appel bloquant à une fonction
        système. Par convention, le code de l'appel doit avoir été mis
        dans le registre \texttt{\$v0} avant l'appel.\\

        \hline

        Synchronisation & \texttt{sync} & Bloque l'exécution tant qu'une seconde
	      ne s'est pas écoulée depuis le dernier appel à~\texttt{sync}
	      (voir \ref{sync}) \\

        \hline
      \end{tabular}
    \end{center}

    De plus, les pseudo-instructions \texttt{li} et \texttt{la} sont supportées
    par notre assembleur.

  \subsection{Assembleur}

    Le langage assembleur supporté par notre simulateur est donc la réunion de
    toutes les instructions susmentionnées. Le fichier source contenant le
    programme doit contenir deux segments :
    
    \begin{itemize}
      \item Un segment \texttt{.text} contenant une suite d'instructions
        éventuellement repérées par des étiquettes. L'exécution du programme
        commence à la première instruction donc il n'est pas nécessaire de
        désigner un point d'entrée.

      \item Un segment \texttt{.data} contenant éventuellement des chaînes de
        caractère étiquetées de la façon suivante :
        \texttt{message: .asciiz "Hello, world!"}
    \end{itemize}

\section{Cas particuliers}

  \subsection{Appels systèmes}\label{syscalls}

    Nous avons implémenté quelques appels systèmes, notamment pour
    pouvoir afficher l'heure dans le programme de la montre. Nous avons choisi
    pour cela de réserver l'adresse mémoire \texttt{0x00000004}. Lorsque cette
    adresse contient une valeur non nulle sur~$32$ bits (numéro de service), le
    simulateur simule l'appel système correspondant, remet à zéro la case
    mémoire puis reprend son exécution normale.

    D'un point de vue matériel, il suffit de copier la valeur du registre
    \texttt{\$v0} dans la RAM à la bonne adresse à chaque occurrence de
    l'instruction~\texttt{syscall}. On stocke aussi~\texttt{\$a0}
    et~\texttt{\$a1} aux adresses~\texttt{0x00000008} et~\texttt{0x00000012}
    pour avoir les éventuels arguments de l'appel système, car on ne peut pas
    y avoir accès étant donné que l'on ne connaît pas les noms de ces registres
    une fois le micro-processeur compilé en une \textit{netlist}.

    L'instruction \texttt{syscall} est encodée de la même façon que dans une
    architecture MIPS réelle, c'est-à-dire par la valeur \texttt{0x0000000c}.

    Nous avons choisi d'implémenter les appels système suivants, qui nous
    paraissent suffisants pour le programme de montre numérique, et
    potentiellement pour exécuter d'autres programmes basiques. Ils respectent
    les conventions exigées par MIPS \cite{mars-syscall}.

    \begin{center}
      \begin{tabular}{|r|c|p{0.33\textwidth}|c|}
        \hline

        \textbf{Nom} & \textbf{Code} & \textbf{Argument} & \textbf{Retour}\\

        \hline\hline

        Afficher un entier & 1 & \texttt{\$a0} (entier à afficher) & --- \\

        \hline

        Afficher une chaîne de caractères & 4 & \texttt{\$a0} (adresse de la
        chaîne). La fin de la chaîne est marquée par le caractère
        ascii~\texttt{\\0} (\texttt{NUL}) & --- \\

        \hline

        Lire un entier & 5 & --- & \texttt{\$v0} (entier lu) \\

        \hline

        Fin du programme & 10 & --- & --- \\

        \hline
      \end{tabular}
    \end{center}

  \subsection{Synchronisation de la montre}\label{sync}

    Pour gérer la synchronisation de la montrer avec l'heure extérieure, nous
    utilisons la même technique : l'adresse mémoire \texttt{0x00000000} contient
    une valeur égale à $0$ si cette case mémoire a été lue depuis moins d'une
    seconde et $1$ si cela fait plus d'une seconde. C'est le simulateur qui
    assure que cette propriété reste vraie.

    Cela permet par exemple de faire des sauts conditionnels pour boucler en
    attendant qu'une seconde se soit écoulée. En outre, en adaptant la fréquence
    à laquelle le simulateur met à jour cette case mémoire, on peut alors
    contrôler la vitesse à laquelle fonctionne le micro-processeur, et, par
    extension, la montre numérique. Cette solution était censée nous permettre 
    de gérer à la fois le cas d'une montre synchronisée avec le temps réel, et le cas d'une montre tournant le plus vite possible. Toutefois, en pratique, notre
    simulateur tourne relativement lentement (sur nos machines) ; et un programme
    de montre numérique requiert parfois plus d'une seconde réelle pour calculer et
    afficher une seconde virtuelle, ce qui rend l'option du temps réel caduque.

    La pseudo-instruction \texttt{sync} a ainsi été implémentée dans ce but.
    Celle-ci crée une boucle dont on ne sort que lorsqu'un $1$ est lu à l'adresse
    \texttt{0x00000002}. Autrement dit (\texttt{\_\_sync\_n} désigne un label unique) :

    \begin{tabular}{rl}
      \texttt{\_\_sync\_n:}
        & \texttt{lw \ \$t0, 0(\$zero)} \\
        & \texttt{beq \$t0, \$zero, sync\_n}
    \end{tabular}

\section{Améliorations possibles}

  Par manque de temps, et à cause de certaines difficultés (pour réaliser des
  opérations avancées, telle que la division, ou encore à cause de la lenteur du
  micro-processeur simulé), nous n'avons malheuresement pas pu réaliser de vrai
  montre numérique, synchronisée et complète. Il s'agit évidemment de la première
  amélioration que nous souhaiterions apporter à ce projet.

  Nous aurions de plus aimé pouvoir apporter les améliorations suivantes :

  \begin{itemize}
    \item Support complet du segment de données \texttt{.data}.
    \item Comme mentionné dans l'introduction, rendre compatible l'assembleur
      MIPS produit à partir d'un langage plus haut niveau.
    \item Implémenter plus d'instructions (par exemple, prendre en compte les
      instructions avec dépassement), et toutes les pseudo instructions.
  \end{itemize}

  \bibliographystyle{plain}
  \bibliography{biblio.bib}

\end{document}
