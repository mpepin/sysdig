let hor s i =
  for k = 0 to 5 do
    print_string (
      if s.[i+7*k] = '1' then " ====  "
      else "       ");
    if k mod 2 = 1 then print_string "  "
  done;
  print_newline ()

let vert s i j pt =
  for k = 0 to 5 do
    print_string begin match (s.[i+7*k], s.[j+7*k]) with
      | ('1', '1') -> "|    | "
      | ('1', '0') -> "|      "
      | ('0', '1') -> "     | "
      | ('0', '0') -> "       "
      | _ -> assert false end;
    if k mod 2 = 1 && k <> 5 then
      print_string (if pt then "o " else "  ")
  done ; print_newline ()

let rec print_hours code=
  hor code 6 ;
  vert code 0 5 false ;
  vert code 0 5 true ;
  hor code 1 ;
  vert code 2 4 true ;
  vert code 2 4 false ;
  hor code 3 ;
  print_newline ()

let update_code code new_code =
  let n = String.length code in
  let nn = String.length new_code in
  (String.sub code 0 (n-nn)) ^ new_code

let get_code input = String.sub input 0 7

let print_date date =
  Format.printf "%d - %d - %d@." date.(0) date.(1) date.(2)

let rec main code date =
  let s = ref (get_code (input_line stdin)) in
  let new_code = ref "" in
  let updates = ref 5 in
  while !updates >= 0 && !s <> "EOF" do
    new_code := !s ^ !new_code;
    decr updates;
    s := input_line stdin
  done;
  new_code := update_code code !new_code;
  let i = ref 0 in
  while !s <> "EOF" do
    date.(!i) <- int_of_string !s;
    incr i;
    s := input_line stdin
  done;
  print_hours !new_code;
  print_date date;
  main !new_code date

(* TODO change value according to input, to synch with the clock *)
(* Optional *)
let c = "101111110111111011111101111110111111011111"
let d = [|17; 1; 2017|]


let _ = input_line stdin
let _ = input_line stdin
let _ = input_line stdin
let _ = input_line stdin
let _ = input_line stdin
let () = main c d
