# Projet du cours de système digital 2016-2017

## Requirements

- Ocamlbuild with Ocamlfind, Ocaml version 4.03 and menhir
- latexmk

## HOWTO

La commande pour compiler le processeur est `make`.

Pour lancer le programme de la montre sur le processeur, taper `make run`.

Pour lancer un programme arbitraire contenu dans `foo.s` sur le processeur,
placer `foo.s` dans `test/asm/` et taper `./run.sh foo`.

### Compiler séparément les différentes parties

Voici la liste des commandes makefile disponibles :

- `make`, `make run` : cf ci dessus.
- `make simulator` : compile uniquement le simulateur.
- `make minijazz` : compile uniquement le compilateur minijazz.
- `make pre_build` assemble les sources minijazz en un seul fichier.
- `make test` : lance une batterie de tests, cf plus bas.
- `make pdf` : génère le rapport du projet à l'aide de `latexmk`.
- `make clean` : nettoie le dépôt des fichiers générés par `make` et
  `make test` uniquement.
- `make fullclean` : Nettoie tout le dépôt, y compris les fichiers générés par
  ocamlbuild et le pdfs.


---


## Structure du dépôt

- `simulator/` : les sources du simulateur.
    - Un parseur de netlist
    - Un parseur d'assembleur MIPS
    - L'ordonnancement et l'exécution du circuit
- `minijazz/` : le compilateur minijazz vers netlist.
- `cpu/` : le code minijazz du processeur lui même.
    - Les différentes parties du processeur sont sous `cpu/src/`. Le point
      d'entrée est dans le fichier `main.mj`.
    - Un script Ocaml qui assemble les différents morceaux du CPU en un seul
      fichier. Cf la section suivante.
- `test/` : des tests unitaires plus ou moins exhaustifs.

Les dossiers `_CPU_build/` et `_test_build/` sont générés automatiquement pour y
stocker les fichiers générés lors de la compilation.

Enfin, le script `run.sh` sert à lancer le simulateur sur un programme
assembleur donné en entrée. Usage : `./run.sh foo` où `test/asm/foo.s` est le
fichier assembleur à charger en ROM.

## Système de build custom pour minijazz

Le script `cpu/pre_build.ml` sert à séparer le code du CPU en plusieurs fichiers
(dans `cpu/src/`). Ces fichiers doivent contenir sur leurs premières lignes la
liste des fichiers dont ils dépendent au format `require foo` (pour signifier
"dépend de `foo.mj`"). Le graphe de dépendance des différents morceaux de
circuit et ensuite construit et les fichiers sont concaténés dans le bon ordre. 

**Attention** : ce n'est **pas** un vrai système de module au sens où il n'y a
pas de namespace : si deux fichiers génèrent des blocs avec le même nom il y
aura conflit.


---


## Tests

Le dossier `test/` contient trois types de tests :

- Des fichiers `.net` pour tester le simulateur (utilisés lors de l'écriture du
  simulateur, plus utiles désormais).
- Des fichiers `.mj` avec pour chacun, des fichiers `.in` et `.out` qui
  correspondent respectivement à des entrées données au circuit à chaque cycle
  et les sorties attendues correspondantes. Le script `test_mj.sh` teste ces
  circuits un à un sur les entrées spécifiées et vérifie la sortie.
- Des fichiers assembleur. Actuellement le script `test_asm.sh` vérifie
  uniquement que les programmes sont chargés sans incident dans la ROM. À terme
  il faudrait que le CPU soit lancé sur ces programmes et qu'on vérifie que
  l'exécution se déroule correctement.

La commande `make test` permet de lancer tous les tests et d'afficher le
résultat.
