open Netlist_ast
open Graph

exception Combinational_cycle

let _read_arg = function
  | Avar i -> [i]
  | Aconst _ -> []

let read_exp eq = match snd eq with
  | Earg arg | Enot arg | Erom (_, _, arg) | Eslice (_, _, arg)
  | Eselect (_, arg) -> _read_arg arg
  | Ereg _ -> []
  | Ebinop (_, a1, a2) | Econcat (a1, a2) ->
      _read_arg a1 @ _read_arg a2
  | Emux (a1, a2, a3) ->
      _read_arg a1 @ _read_arg a2 @ _read_arg a3
  | Eram (_, _, a1, a2, a3, a4) ->
      _read_arg a1 @ _read_arg a2 @ _read_arg a3
      @ _read_arg a4


let _build_dep_graph p =
  (* Graph init *)
  let g = mk_graph () in
  let equations = Hashtbl.create 1024 in
  List.iter (fun eq -> Hashtbl.add equations (fst eq) eq;
                       add_node g eq)
            p.p_eqs;
  (* Adding edges *)
  let my_add_node eq =
    let ids = read_exp eq in
    let my_add_edge id =
      if List.mem id p.p_inputs then ()
      else try
        let eq' = Hashtbl.find equations id in
        add_edge g eq eq'
      with Not_found -> () in
    List.iter my_add_edge ids in
  List.iter my_add_node p.p_eqs;
  g


let schedule verbose p =
  if verbose then print_endline "Building dependency graph...";
  let g = _build_dep_graph p in
  if verbose then print_endline "Tracking combinatorial cycles...";
  if has_cycle g then raise Combinational_cycle;
  if verbose then print_endline "Scheduling program...";
  { p with p_eqs = topological g }
