open Asm_ast

(***
 * Preliminary tools
 ***)

(** Phantom type to avoid confusion *)
type ram_t
type rom_t
type 'a env = {
  ram: Ram.t ;
  mutable addr: int ; (* address of the next instruction *)
  labels: (string, int) Hashtbl.t
}
let mkenv ram init = {
  ram = ram ;
  addr = init ;
  labels = Hashtbl.create 17 ;
}

let incaddr env = env.addr <- env.addr + 4

exception UnknownLabel of string
let find_label env label =
  try (Hashtbl.find env.labels label)/4
  with Not_found -> raise @@ UnknownLabel label

(* Conversion of integers into boolean arrays *)

let zeros n = Array.make n false

let bits_of_int_unsigned size n =
  let bits = zeros size in
  let k = ref n in
  for i = size-1 downto 0 do
    bits.(i) <- Utils.bool_of_int (!k mod 2);
    k := !k lsr 1
  done;
  bits

let bits_of_int_signed size n = 
  let sign = [| n < 0 |] in
  let y = if n < 0 then 1 lsl (size-1) else 0 in
  Array.concat [sign; bits_of_int_unsigned (size-1) (y+n)]

let boiu = bits_of_int_unsigned
let bois = bits_of_int_signed

(* Registers *)

let _regs =
  let tbl = Hashtbl.create 11 in
  let f i s = Hashtbl.add tbl s (boiu 5 i) in
  let () = List.iteri f [
    "zero"; "at"; "v0"; "v1"; "a0"; "a1"; "a2"; "a3";
    "t0"; "t1"; "t2"; "t3"; "t4"; "t5"; "t6"; "t7";
    "s0"; "s1"; "s2"; "s3"; "s4"; "s5"; "s6"; "s7";
    "t8"; "t9"; "k0"; "k1"; "gp"; "sp"; "fp"; "ra"] in
  tbl
let reg s =
  try Hashtbl.find _regs s
  with Not_found -> raise @@ Invalid_argument ("Not a register: "^s) 


(***
 * .text section handling 
 ***)


let write_instruction (rom_env : rom_t env) (ram_env : ram_t env) instr =
  begin match instr with
      (*
         R format: opcode rs rt rd shamt funct
      *)
      | Rinstr1(r, funct) ->
          (* Determines if the register code should be stored in position rs or
             in position rd in the binary encoding. *)
          begin match funct with
          | 0x08 -> (* rs *)
              [zeros 6; reg r; zeros 15; boiu 6 funct]
          | 0x10 | 0x12 -> (* rd *)
              [zeros 16; reg r; zeros 5; boiu 6 funct]
          | _ -> 
              raise @@ Invalid_argument (Printf.sprintf
                  "Asm_writer.write_instruction received a Rinstr1 \
                   instruction with the invalid funct code %d." funct)
          end
      | Rinstr2(r1, r2, funct) ->
          [zeros 6; reg r1; reg r2; zeros 10; boiu 6 funct]
      | Rinstr3(r1, r2, r3, funct) ->
          [zeros 6; reg r2; reg r3; reg r1; zeros 5; boiu 6 funct]
      | Rinstrs(r1, r2, shamt, funct) ->
          [zeros 11; reg r2; reg r1; boiu 5 shamt; boiu 6 funct]
      (*
         I format: opcode rs rt immediate
      *)
      | Iinstr1(opcode, r, imm) ->
          [boiu 6 opcode; zeros 5; reg r; bois 16 imm]
      | Iinstr2(opcode, r1, r2, imm) ->
          if opcode = 4 || opcode = 5 then
            [boiu 6 opcode; reg r1; reg r2; bois 16 imm]
          else
            [boiu 6 opcode; reg r2; reg r1; bois 16 imm]
      | Iinstrl2(opcode, r1, r2, label) ->
          let addr = try find_label rom_env label
                     with UnknownLabel _ ->
                         4*(find_label ram_env label) in
          [boiu 6 opcode; reg r2; reg r1; boiu 16 addr]
      | Iinstrofs(opcode, r1, r2, ofs) ->
          [boiu 6 opcode; reg r2; reg r1; boiu 16 ofs]
      | Lui_l(r, label) -> 
          let addr =  try find_label rom_env label
                      with UnknownLabel _ ->
                          4*(find_label ram_env label) in
          [boiu 6 0xF; zeros 5; reg r; boiu 16 (addr lsr 16)]
      (*
         J format: opcode address
      *)
      | Jinstr(opcode, label) ->
          let addr = find_label rom_env label in
          [boiu 6 opcode; boiu 26 addr]
      (*
         Special instructions
      *)
      | Nop -> [zeros 32]
      | Syscall ->
          [zeros 28; boiu 4 12]
  end
  |> Array.concat


let write_textline (rom_env : rom_t env) (ram_env : ram_t env) = function
  | TLabel _ -> ()
  | TInstr i ->
      let instr = write_instruction rom_env ram_env i in
      Ram.write_word rom_env.ram rom_env.addr instr;
      incaddr rom_env


let find_text_labels (env : rom_t env) = function
  | TLabel l -> Hashtbl.replace env.labels l env.addr
  | TInstr _ -> incaddr env


(***
 * .data section handling
 ***)


let write_dataline (env : ram_t env) = function Asciiz (label, str) ->
  Hashtbl.replace env.labels label env.addr;
  let write_char c =
    Ram.write_byte env.ram env.addr (boiu 8 @@ int_of_char c);
    env.addr <- env.addr + 1 in
  String.iter write_char str;
  write_char '\000';
  env.addr <- env.addr + 1;
  if env.addr mod 4 <> 0 then
    env.addr <- 4*(env.addr/4) + 4


(***
 * Main function
 ***)


let write_on_rom rom ram prog =
  let (rom_env : rom_t env) = mkenv rom 0 in
  let (ram_env : ram_t env) = mkenv ram 16 in
  List.iter (write_dataline ram_env) prog.data;
  let program_start = rom_env.addr in
  List.iter (find_text_labels rom_env) prog.text;
  rom_env.addr <- program_start;
  List.iter (write_textline rom_env ram_env) prog.text;
  (rom_env.addr, ram_env.addr)
