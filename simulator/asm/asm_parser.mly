
(* Analyseur syntaxique pour mini-scala *)

%{
    open Asm_ast

    let fresh_sync_label =
      let i = ref (-1) in
      function () -> 
        incr i;
        Printf.sprintf "__sync_%d" !i
%}

(* Déclaration des tokens *)

%token <string> REG LABEL LABELREF STRING
%token <int> IINSTR RINSTR JINSTR NUM
%token EOF EOL COMMA LP RP
%token TEXT DATA NOP SYSCALL ASCIIZ LA LI SYNC LUI


(* Points d'entrée de la grammaire *)
%start file

(* Types des valeurs renvoyées par l'analyseur syntaxique *)
%type <Asm_ast.file> file

%%

_instr:
  (* R format *)
  | i = RINSTR ; r = REG
    { Rinstr1(r, i) }
  | i = RINSTR ; r1 = REG ; COMMA ; r2 = REG 
    { Rinstr2(r1, r2, i) }
  | i = RINSTR ; r1 = REG ; COMMA ; r2 = REG ; COMMA ; r3 = REG
    { Rinstr3(r1, r2, r3, i) }
  | i = RINSTR ; r1 = REG ; COMMA ; r2 = REG ; COMMA ; shamt = NUM
    { Rinstrs(r1, r2, shamt, i) }
  (* I format *)
  | i = IINSTR ; r = REG ; COMMA ; n = NUM
    { Iinstr1(i, r, n) }
  | i = IINSTR ; r1 = REG ; COMMA ; r2 = REG ; COMMA ; n = NUM
    { Iinstr2(i, r1, r2, n) }
  | i = IINSTR ; r1 = REG ; COMMA ; r2 = REG ; COMMA ; addr = LABELREF
    { Iinstrl2(i, r1, r2, addr) }
  | i = IINSTR ; r1 = REG ; COMMA ; n = NUM ; LP ; r2 = REG ; RP
    { Iinstrofs(i, r1, r2, n) } 
  | LUI ; r = REG ; COMMA ; addr = LABELREF
    { Lui_l (r, addr) }
  | LUI ; r = REG ; COMMA ; n = NUM
    { Iinstr1(0xF, r, n) }
  (* J format *)
  | i = JINSTR ; addr = LABELREF
    { Jinstr(i, addr) }
  (* Special instructions *)
  | NOP { Nop }
  | SYSCALL { Syscall }

eols: nonempty_list(EOL) { () }

textline:
  | i = _instr ; eols { [TInstr i] }
  | l = LABEL ; eols { [TLabel l] }
  (* Pseudo instructions *)
  | LA ; r = REG ; COMMA ; addr = LABELREF ; eols
    { [ TInstr (Lui_l (r, addr)) ;
        TInstr (Iinstrl2(0xD, r, r, addr)) ] }
  | LI ; r = REG ; COMMA ; n = NUM ; eols
    { [ TInstr (Iinstr1(0xF, r, n lsr 16)) ;
        TInstr (Iinstr2(0xD, r, r, n mod (1 lsl 16))) ] }
  | SYNC ; eols
    { let label = fresh_sync_label () in
      [ TLabel label ; 
        TInstr (Iinstr2(0x23, "t0", "zero", 0)) ; 
        TInstr (Iinstrl2(0x4, "t0", "zero", label)) ] }

dataline: l = LABEL ; ASCIIZ ; s = STRING ; eols { Asciiz (l, s) }

text:
  | { [] }
  | tl = textline ; txt = text { tl @ txt }

file:
  TEXT ; eols ; text = text ;
  DATA ; eols ; data = list(dataline) ;
  EOF
  {{ text = text ; data = data }}
