type reg = string

type instruction =
  | Rinstr1 of reg * int
  | Rinstr2 of reg * reg * int
  | Rinstr3 of reg * reg * reg * int
  | Rinstrs of reg * reg * int * int
  (** Last argument is the funct code *)

  | Iinstr1 of int * reg * int
  | Iinstr2 of int * reg * reg * int
  | Iinstrl2 of int * reg * reg * string
  | Lui_l of reg * string (* lui instruction with label *)
  (** First argument is the opcode ; last argument is the immediate value *)

  | Iinstrofs of int * reg * reg * int
  (** Same with immediate value being an offset : C($reg) *)

  | Jinstr of int * string
  (** Fisrt argument is opcode *)
  
  | Nop | Syscall
  (** Special instructions *)

type text =
  | TInstr of instruction
  | TLabel of string

type data = Asciiz of string * string

type file = {
  text: text list ;
  data: data list
}
