{
    open Asm_parser
    open Lexing

    exception Lexing_error of string
    let error msg = raise (Lexing_error msg)

    let newline lexbuf =
        let pos = lexbuf.lex_curr_p in
        lexbuf.lex_curr_p <- {
            pos with pos_lnum = pos.pos_lnum + 1;
            pos_bol = pos.pos_cnum
        }

    (* Instructions *)
    let instructions = Hashtbl.create 35
    let () = List.iter (fun (kw, tok) -> Hashtbl.add instructions kw tok)
                       [
                        (* Arithmetic *)
                        "add",   RINSTR 0x20;
                        "addi",  IINSTR 0x8 ;
                        "sub",   RINSTR 0x22;
                        "mult",  RINSTR 0x18;
                        "div",   RINSTR 0x1A;
                        (* Bitwise logic *) 
                        "or",    RINSTR 0x25;
                        "ori",   IINSTR 0xD ;
                        "and",   RINSTR 0x24;
                        "andi",  IINSTR 0xC ;
                        "xor",   RINSTR 0x26;
                        "xori",  IINSTR 0xE ;
                        (* Comparison *)
                        "slt",   RINSTR 0x2A;
                        "slti",  IINSTR 0xA ;
                        (* Bitwise shift *)
                        "sllv",  RINSTR 0x04;
                        "sll",   RINSTR 0x00;
                        "srlv",  RINSTR 0x06;
                        "srl",   RINSTR 0x02;
                        "srav",  RINSTR 0x07;
                        "sra",   RINSTR 0x03;
                        (* RAM access *)
                        "lw",    IINSTR 0x23;
                        "lh",    IINSTR 0x21;
                        "lb",    IINSTR 0x20;
                        "sw",    IINSTR 0x2B;
                        "sh",    IINSTR 0x29;
                        "sb",    IINSTR 0x28;
                        "lui",   LUI;
                        "mfhi",  RINSTR 0x10;
                        "mflo",  RINSTR 0x12;
                        (* Jump / branch *)
                        "beq",   IINSTR 0x4 ;
                        "bne",   IINSTR 0x5 ;
                        "j",     JINSTR 0x2 ;
                        "jr",    RINSTR 0x08;
                        "jal",   JINSTR 0x3 ;
                        (* Pseudo instructions *)
                        "la",    LA ;
                        "li",    LI ;
                        (* Special *)
                        "nop",      NOP;
                        "syscall",  SYSCALL;
                        "sync",     SYNC;
                       ]
      let read_instr id =
        try Hashtbl.find instructions id
        with Not_found -> LABELREF id

    (* keywords *)
      let read_kw = function
          | "text" -> TEXT
          | "data" -> DATA
          | "asciiz" -> ASCIIZ
          | s -> error @@ Printf.sprintf "Unknown keyword: %s" s

    (* Symbols *)
    let read_sym = function
      | '(' -> LP
      | ')' -> RP
      | ',' -> COMMA
      | _ -> assert false
                        
}
    let digit = ['0' - '9']
    let number = '-'? digit+
    let letter = ['a' - 'z']
    let word = (letter | '_') (letter | digit | '_')*
    let sym = '(' | ')' | ','

rule token = parse
    | [' ' '\t']+         { token lexbuf }
    | ['\n' '\r']         { newline lexbuf; EOL }
    | '.' (word as w)     { read_kw w }
    | '$' (word as r)     { REG r }
    | (word as l) ':'     { LABEL l }
    | word as instr       { read_instr instr }
    | sym as s            { read_sym s }
    | number as n         { NUM (int_of_string n) }
    | '"'                 { lex_string "" lexbuf }
    | '#'                 { comment lexbuf }
    | eof                 { EOF }
    | _ as c              { error @@ Printf.sprintf "Unknown character: %c" c } 

and lex_string s = parse
    | '\n'                { newline lexbuf ; lex_string (s^"\n") lexbuf }
    | '"'                 { STRING s }
    | '\\' 't'            { lex_string (s^"\t") lexbuf }
    | '\\' 'n'            { lex_string (s^"\n") lexbuf }
    | '\\' '\\'           { lex_string (s^"\\") lexbuf }
    | '\\' '"'            { lex_string (s^"\"") lexbuf }
    | '\\' (_ as c)       { error @@ Printf.sprintf
                                       "Illegal escaped char: %c" c}
    | _ as c              { lex_string (Printf.sprintf "%s%c" s c) lexbuf }
    | eof                 { error "EOF in a string" }

and comment = parse
    | ['\n' '\r']         { newline lexbuf; EOL }
    | eof                 { EOF }
    | [^'\n' '\r']        { comment lexbuf }
