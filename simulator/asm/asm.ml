let ram_size = ref 4096
let print = ref false

let positive = function
  | n when n < 0 -> 0
  | n -> n

let compile filename =
  let rom = Ram.init !ram_size in
  let ram = Ram.init !ram_size in
  let (rom_len, ram_len) = Asm_compile.compile rom ram filename in
  if !print then begin
    print_endline "== ROM ==";
    Ram.print_ram stdout ~range:(0, positive @@ rom_len - 1) rom;
    print_endline "== RAM ==";
    Ram.print_ram stdout ~range:(0, positive @@ ram_len - 1) ram
  end


let () =
  Arg.parse
    ["-s", Arg.Int ((:=) ram_size), "The RAM size, default is 1024";
     "-p", Arg.Set print, "Print the binary code produced, if not set the \
                           will only check that the asm file can be compiled"]
    compile
    "usage: asm.native [-p] [-s <size>] <asm.file>"
