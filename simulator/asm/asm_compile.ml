open Lexing

let localisation pos filename =
  let l = pos.pos_lnum in
  let c = pos.pos_cnum - pos.pos_bol + 1 in
  Format.eprintf
      "File \"%s\", line %d, characters %d-%d:\n"
      filename l (c-1) c

let compile rom ram filename =
  let ic = open_in filename in
  let buffer = from_channel ic in
  try
    let prog = Asm_parser.file Asm_lexer.token buffer in
    Asm_writer.write_on_rom rom ram prog
  with
    | Asm_parser.Error ->
        localisation (lexeme_start_p buffer) filename;
        Format.eprintf "Syntax error@.";
        close_in ic;
        exit 1
    | err -> 
        close_in ic;
        raise err
