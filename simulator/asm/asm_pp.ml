open Printf
open Asm_ast

let print_instruction oc = function
  (* R format *)
  | Rinstr1(r, f) ->
      fprintf oc "    (00,%x)\t$%s\n" f r
  | Rinstr2(r1, r2, f) ->
      fprintf oc "    (00,%x)\t$%s,$%s\n" f r1 r2
  | Rinstr3(r1, r2, r3, f) ->
      fprintf oc "    (00,%x)\t$%s,$%s,$%s\n" f r1 r2 r3
  | Rinstrs(r1, r2, shamt, f) ->
      fprintf oc "    (00,%x)\t$%s,$%s,%d" f r1 r2 shamt
  (* I format *)
  | Iinstr1(i, r, n) ->
      fprintf oc "    (%x,00)\t$%s,%d\n" i r n
  | Iinstr2(i, r1, r2, n) ->
      fprintf oc "    (%x,00)\t$%s,$%s,%d\n" i r1 r2 n
  | Iinstrl2(i, r1, r2, addr) ->
      fprintf oc "    (%x,00)\t$%s,$%s,%s\n" i r1 r2 addr
  | Iinstrofs(i, r1, r2, ofs) ->
      fprintf oc "    (%x,00)\t$%s,%d($%s)\n" i r1 ofs r2
  (* J format *)
  | Jinstr(i, addr) ->
      fprintf oc "    (%x,00)\t%s\n" i addr
  (* Pseudo instructions *)
  | La(r, addr) ->
      fprintf oc "    la\t%s,%s\n" r addr
  | Li(r, n) ->
      fprintf oc "    li\t%s,%d\n" r n
  (* Special instructions *)
  | Nop ->      fprintf oc "    nop\n"
  | Syscall ->  fprintf oc "    syscall\n"
  | Sync ->     fprintf oc "    sync\n"


let print_textline oc = function
  | TLabel l -> fprintf oc "  %s:\n" l
  | TInstr i -> print_instruction oc i

let print_data oc = function Asciiz (l, s) ->
  fprintf oc "  %s: .asciiz \"%s\"\n" l s

let print_file oc f =
  fprintf oc ".text\n";
  List.iter (print_textline oc) f.text;
  fprintf oc ".data\n";
  List.iter (print_data oc) f.data;
  fprintf oc "EOF\n"

