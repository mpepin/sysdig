exception Cycle
type mark = NotVisited | InProgress | Visited

type 'a graph =
    { g_nodes : ('a, 'a node) Hashtbl.t  }
and 'a node = {
  n_label : 'a;
  mutable n_mark : mark;
  mutable n_link_to : 'a node list;
  mutable n_linked_by : 'a node list;
}

let is_visited n = n.n_mark = Visited

let mk_graph () = { g_nodes = Hashtbl.create 1024 }

let add_node g x =
  let n = {
    n_label = x;
    n_mark = NotVisited;
    n_link_to = [];
    n_linked_by = []} in
  Hashtbl.add g.g_nodes x n

let node_for_label g x =
  Hashtbl.find g.g_nodes x

let add_edge g id1 id2 =
  let n1 = node_for_label g id1 in
  let n2 = node_for_label g id2 in
  n1.n_link_to <- n2::n1.n_link_to;
  n2.n_linked_by <- n1::n2.n_linked_by

let clear_marks g =
  Hashtbl.iter (fun _ n -> n.n_mark <- NotVisited) g.g_nodes

let find_roots g =
  Hashtbl.fold (fun _id n l ->
                  if n.n_linked_by = [] then n::l
                  else l)
               g.g_nodes
               []

let has_cycle g =
  clear_marks g;
  let rec walk node = match node.n_mark with
    | Visited -> ()
    | InProgress -> raise Cycle
    | NotVisited ->
        node.n_mark <- InProgress;
        List.iter walk node.n_link_to;
        node.n_mark <- Visited in
  try Hashtbl.iter (fun _ node -> walk node) g.g_nodes; false
  with Cycle -> true

let topological g =
  clear_marks g;
  let tbl = Hashtbl.copy g.g_nodes in
  let count = ref (Hashtbl.length tbl) in
  let select id node res =
    if (node.n_mark = NotVisited &&
        List.for_all (fun n -> n.n_mark = Visited) node.n_link_to) then begin
      node.n_mark <- Visited;
      decr count;
      Hashtbl.remove tbl id;
      (node.n_label::res)
    end else res in
  let rec sort res =
    if !count > 0 then
      sort (Hashtbl.fold select tbl res)
    else res in
  List.rev @@ sort []
