(** Prints an integer on stdout and returns 0 *)
let sys_print_int a0 =
  Format.printf "%d@?" a0;
  0

(** Reads an integer from stdin and returns it *)
let sys_input_int () =
  let line = input_line stdin in
  int_of_string line

(** Prints the string starting at start_addr in RAM on stdout.
    The string must end with \000. *)
let sys_print_str ram start_addr =
  let addr = ref start_addr in
  let current_char = ref 0 in
  let read_char () =
    current_char := (Ram.int_of_bits @@ Ram.get_byte ram !addr);
    incr addr in    
  while (read_char () ; !current_char <> 0) do
    Printf.printf "%c" (char_of_int !current_char)
  done;
  flush stdout;
  0

(** Stops the simulator execution, maybe a little bit violent but… It will do
    the trick. *)
let sys_exit () = exit 0

(** Resets the content of $v0, $a0, $a1 in RAM *)
let reset ram =
  let reset_aux addr = Ram.write_word ram addr (Ram.bits_of_int 32 0) in
  List.iter reset_aux [4; 8; 12]

(** The switch for syscalls:
    - The values of $a0, $a1, $v0 are read from the RAM
    - The syscall is executed
    - The return value is written at the right place in RAM
*)
let syscall ram =
  let a0 = Ram.int_of_bits @@ Ram.get_word ram 8 in
  let v0 = Ram.int_of_bits @@ Ram.get_word ram 4 in
  match v0 with
    | 0  -> 0 (* a dummy syscall *)
    | 1  -> reset ram ; sys_print_int a0
    | 4  -> reset ram ; sys_print_str ram a0
    | 5  -> reset ram ; sys_input_int ()
    | 10 -> reset ram ; sys_exit ()
    | n  -> failwith @@
        Printf.sprintf "Invalid syscall code: %d" n
