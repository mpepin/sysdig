open Netlist_ast
open Utils

module Env = struct
  type ident = string
  type t = {  vars: (ident, value) Hashtbl.t ;
              regs: (ident, value) Hashtbl.t ;
              ram: Ram.t ;
              rom: Ram.t }

  let init asm_file ram_size rom_size =
    let rom = Ram.init rom_size in
    let ram = Ram.init ram_size in
    if String.length asm_file > 0 then
      ignore (Asm_compile.compile rom ram asm_file : int * int);
    {
      vars = Hashtbl.create 17 ;
      regs = Hashtbl.create 17 ;
      ram = ram ;
      rom = rom
    }

  (* Variables manipulation *)
  let set_var env id v = Hashtbl.replace env.vars id v
  let get_var env id =
    try Hashtbl.find env.vars id
    with Not_found -> failwith @@ Printf.sprintf
        "Variable %s not found" id

  (* Registers manipulation *)
  let get_reg env id =
    try
      Hashtbl.find env.regs id
    with Not_found ->
      Hashtbl.add env.regs id (VBit false);
      VBit false
  let update_regs env =
    let filter id _ = Some (Hashtbl.find env.vars id) in
    Hashtbl.filter_map_inplace filter env.regs

  (* The special case of address 0x00000002 *)
  let set_tick_byte env v =
    Ram.write_word env.ram 0 (Ram.bits_of_int 32 v)

  (* RAM/ROM manipulation *)
  let wrong_size size = Ram.(
    Invalid_RAM_access (Printf.sprintf "Invalid size: %d" size))
    |> raise
  let _get word_size mem_unit addr = match word_size with
    | 8 ->  Ram.get_byte mem_unit (Ram.int_of_bits addr)
    | 16 -> Ram.get_halfword mem_unit (Ram.int_of_bits addr)
    | 32 -> Ram.get_word mem_unit (Ram.int_of_bits addr)
    | n -> wrong_size n 
  let get_rom word_size env addr = 
    let instr = _get word_size env.rom addr in
    instr
  let get_ram word_size env addr =
    let value = _get word_size env.ram addr in
    if Ram.int_of_bits addr = 0 && Ram.int_of_bits value = 1 then
      set_tick_byte env 0;
    value
  let write_on_ram word_size env bin_addr data =
    let addr = Ram.int_of_bits bin_addr in
    match word_size with
      | 8 ->  Ram.write_byte     env.ram addr data
      | 16 -> Ram.write_halfword env.ram addr data
      | 32 -> Ram.write_word     env.ram addr data
      | n -> wrong_size n

  (* Accessors *)
  let ram env = env.ram
end


(* Parsing of inputs *)
let rec parse_arg in_c env typs arg =
  Printf.printf "%s ? " arg; flush stdout;
  let input = input_line in_c in
  let retry () =
    Printf.printf "Wrong input.\n";
    parse_arg in_c env typs arg in
  match TMap.find arg typs with
    | TBit ->
        if String.length input = 1 then
          Env.set_var env arg @@ VBit (bool_of_char input.[0])
        else retry ()
    | TBitArray n ->
        if String.length input = n then
          Env.set_var env arg @@ VBitArray (barray_of_string input)
        else retry ()


(* A printer for values *)
let str_of_val = function
  | VBit b -> string_of_int @@ int_of_bool b
  | VBitArray arr ->
      String.init (Array.length arr)
                  (fun i -> if arr.(i) then '1' else '0')


let get_bit = function
  | VBit b -> b
  | VBitArray arr ->
      if Array.length arr = 1 then
        arr.(0)
      else failarg "Bit expected"
let get_array = function
  | VBit b -> [|b|]
  | VBitArray arr -> arr


(* Interpreting an argument *)
let interp_arg env = function
  | Avar id -> Env.get_var env id
  | Aconst v -> v


(* Interpreting an expression *)
let interp_exp env = function
  | Earg arg -> interp_arg env arg
  | Ereg ident -> Env.get_reg env ident
  | Enot arg ->
      let b = get_bit @@ interp_arg env arg in
      VBit (not b)
  | Ebinop (binop, arg1, arg2) ->
      let b1 = get_bit @@ interp_arg env arg1 in
      let b2 = get_bit @@ interp_arg env arg2 in
      VBit (match binop with
              | Or   -> b1 || b2
              | Xor  -> (b1 && not b2) || (b2 && not b1)
              | And  -> b1 && b2
              | Nand -> not (b1 && b2))
  | Emux (arg0, arg1, arg2) ->
      if get_bit @@ interp_arg env arg0 then
        interp_arg env arg1
      else
        interp_arg env arg2
  | Erom (_, word_size, arg) ->
      let addr = get_array @@ interp_arg env arg in
      VBitArray (Env.get_rom word_size env addr)
  | Eram (_, word_size, arg1, arg2, arg3, arg4) ->
      let r_addr = get_array @@ interp_arg env arg1 in
      let result = Env.get_ram word_size env r_addr in
      if get_bit @@ interp_arg env arg2 then (
        let w_addr = get_array @@ interp_arg env arg3 in
        let data = get_array @@ interp_arg env arg4 in
        Env.write_on_ram word_size env w_addr data;
      );
      VBitArray result
  | Econcat (arg1, arg2) ->
      let arr1 = get_array @@ interp_arg env arg1 in
      let arr2 = get_array @@ interp_arg env arg2 in
      VBitArray (Array.concat [arr1; arr2])
  | Eslice (i, j, arg) ->
      let arr = get_array @@ interp_arg env arg in
      VBitArray (Array.init (j-i+1) (fun k -> arr.(i+k)))
  | Eselect (i, arg) ->
      let arr = get_array @@ interp_arg env arg in
      VBit arr.(i)


(* The main interpreter *)
let interp asm_file n script_mode input_c output_c p =
  (* Initiating memory *)
  let env = Env.init asm_file 4096 4096 in
  let tick = ref @@ Unix.gettimeofday() in
  (* Main loop *)
  let loop = ref 0 in
  while !loop <> n do
    incr loop;
    if List.length p.p_inputs > 0 then Printf.printf "Step %d:\n" @@ !loop;
    (* Parsing inputs *)
    begin try
      List.iter (parse_arg input_c env p.p_vars) p.p_inputs
    with End_of_file -> print_endline "" ; exit 0 end;
    (* Computing the equations *)
    List.iter (fun (id, exp) -> Env.set_var env id (interp_exp env exp))
              p.p_eqs;
    (* Updating the registers *)
    Env.update_regs env;
    (* Printing the results *)
    List.iter
      (fun id ->
        let value = Env.get_var env id in
        if not script_mode then
          Printf.printf "=> %s = %s\n" id @@ str_of_val value
        else
          let expected = input_line output_c in
          let result = str_of_val value in
          if expected <> result then begin
            Format.eprintf "Unexpected output at %d-th iteration:\n" !loop;
            Format.eprintf "%s instead of %s.@." result expected;
            exit 1 end
      )
      p.p_outputs;
    (* Handling the sync property *)
    let now = Unix.gettimeofday() in
    if now -. !tick >= 1. then begin
      tick := now;
      Env.set_tick_byte env 1
    end;
    (* Handling system calls *)
    let return_code = Syscall.syscall (Env.ram env) in
    let return_code_bin = Ram.bits_of_int 32 return_code in
    Env.write_on_ram 32 env (Ram.bits_of_int 8 4) return_code_bin;
    flush stdout
  done
