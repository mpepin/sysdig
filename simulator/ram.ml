open Utils


type t = bool array array
exception Invalid_RAM_access of string

let array_err s n = raise @@ Invalid_RAM_access
  (Printf.sprintf "%s: out of bouds (%d)" s n) 

let init size =
  if size mod 4 = 0 then
    Array.make_matrix size 8 false
  else raise @@
    Invalid_RAM_access "RAM.init: RAM size must be a multiple of 4"


let int_of_bits bits =
  Array.fold_left (fun n b -> (n lsl 1) + (int_of_bool b)) 0 bits

let rec _bits_of_int bits i = function
  | 0 -> ()
  | n ->
      if i >= 0 then begin
        bits.(i) <- n mod 2 <> 0;
        _bits_of_int bits (i-1) (n/2)
      end
let bits_of_int size n =
  let bits = Array.make size false in
  _bits_of_int bits (size-1) n;
  bits



let get_byte ram addr =
  try ram.(addr)
  with Invalid_argument _ -> array_err "Ram.get_byte" addr

let get_halfword ram addr =
  if addr mod 2 = 0 then
    try Array.concat [ram.(addr) ; ram.(addr+1)]
    with Invalid_argument _ -> array_err "Ram.get_halfword" addr
  else
    raise @@ Invalid_RAM_access (
      Printf.sprintf "Address must be even (%d)." addr)

let get_word ram addr = 
  if addr mod 4 = 0 then
    try Array.concat [ram.(addr) ; ram.(addr+1) ; ram.(addr+2) ; ram.(addr+3)]
    with Invalid_argument _ -> array_err "Ram.get_word" addr
  else
    raise @@ Invalid_RAM_access (
      Printf.sprintf "Address must be a multiple of 4 (%d)." addr)


let write_byte ram addr b =
  if Array.length b = 8 then
    try ram.(addr) <- Array.copy(b)
    with Invalid_argument _ -> array_err "Ram.write_byte" addr
  else raise @@ Invalid_RAM_access "A byte must be 8 bits"


let write_halfword ram addr hw =
  try 
    if addr mod 2 = 0 then
      if Array.length hw = 16 then begin
        let sub = Array.sub hw in
        ram.(addr)    <- sub 0 8;
        ram.(addr+1)  <- sub 8 8
      end else
        raise @@ Invalid_RAM_access "A halfword must be 16 bits"
    else
      raise @@ Invalid_RAM_access (
        Printf.sprintf "Address must be even (%d)." addr)
  with Invalid_argument _ -> array_err "Ram.write_halfword" addr


let write_word ram addr w =
  try
    if addr mod 4 = 0 then begin
      let sub = Array.sub w in
      ram.(addr)    <- sub 0 8;
      ram.(addr+1)  <- sub 8 8;
      ram.(addr+2)  <- sub 16 8;
      ram.(addr+3)  <- sub 24 8  
    end else
      raise @@ Invalid_RAM_access (
        Printf.sprintf "Address must be a multiple of 4 (%d)." addr)
  with Invalid_argument _ -> array_err "Ram.write_word" addr


let print_ram oc ?(range=(0,-1)) ram =
  let (i0, imax) = range in
  let print_bit b = Printf.fprintf oc "%c" (if b then '1' else '0') in
  let print_byte addr byte =
    Printf.fprintf oc "0x%.8X  " addr;
    Array.iter print_bit byte;
    Printf.fprintf oc "\n" in
  if imax = -1 then
    Array.iteri print_byte ram
  else
    for i = i0 to imax do
      print_byte i ram.(i)
    done
