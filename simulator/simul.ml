(* Global options *)
let print_only = ref false
let number_steps = ref (-1)
let asm_file = ref ""
let verbose = ref false

(* Script mode *)
let input_spec = ref ""
let output_spec = ref ""
let script_mode () =
  if String.length !input_spec > 0 then
    if String.length !output_spec > 0 then
      true
    else
      let () = Format.eprintf "--output must be set@." in
      exit 2
  else
    if String.length !output_spec > 0 then
      let () = Format.eprintf "--input must be set@." in
      exit 2
    else false


let compile filename =
  let script = script_mode () in
  let input, output = if script then
      open_in !input_spec, open_in !output_spec
    else
      stdin, stdin
  in
  try
    let p = Netlist.read_file filename in
    let out_name = (Filename.chop_suffix filename ".net") ^ "_sch.net" in
    let out = open_out out_name in
    let close_all () =
      close_out out
    in
    begin try
        let p = Scheduler.schedule !verbose p in
        Netlist_printer.print_program out p;
        close_all ();
        if not !print_only then begin
          if !verbose then print_endline "Starting circuit";
          Interpreter.interp !asm_file !number_steps script input output p
        end
      with
        | Scheduler.Combinational_cycle ->
            Format.eprintf "The netlist has a combinatory cycle.@.";
            close_all (); exit 2
    end;
  with
    | Netlist.Parse_error s -> Format.eprintf "An error accurred: %s@." s; exit 2

let () =
  Arg.parse
    ["-print", Arg.Set print_only, "Only print the result of scheduling";
     "-n", Arg.Set_int number_steps, "Number of steps to simulate";
     "-v", Arg.Set verbose, "Verbose mode";
     "--asm", Arg.Set_string asm_file,
        "If specified, loads the assembly program written in the given file \
         into the circuit's ROM";
     "--input", Arg.Set_string input_spec, 
        "Specifies a file from which the input data will be read";
     "--output", Arg.Set_string output_spec,
        "Specifies a file from which the expected output will be read"]
    compile
    "usage: simul.native [-print] [-n <nb>] [--asm <asm file>] \n\
                         --input <file> --output <file> \n\
                         <netlist>"
