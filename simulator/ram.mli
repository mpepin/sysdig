type t
(** Abstract representation a RAM unit. *)

exception Invalid_RAM_access of string
(** Exception raised when something go wrong during a RAM access. *)

val init : int -> t
(** [init size] instantiates a fresh RAM unit of size [size], [size] being the
    number of addresses, it must be a multiple of 4. *)

val int_of_bits : bool array -> int
(** Converts a binary integer represented as an array of bits ([true] or
    [false]) into an integer.

    [int_of_bits [|true; false|] = 2] *)

val bits_of_int : int -> int -> bool array
(** Conversion inverse de {!int_of_bits}. Le premier argument est la taille du
    mot binaire désiré, les dépassements sont ignorés. *)


(** {2 Reading} *)


val get_byte : t -> int -> bool array
(** [get_byte ram addr] returns the byte stored at address [addr]. *)

val get_halfword : t -> int -> bool array
(** Same as {!get_byte} but checks that the address is even. *)

val get_word : t -> int -> bool array
(** Same as {!get_byte} but checks that the address is a multiple of 4. *)


(** {2 Writing} *)

val write_byte : t -> int -> bool array -> unit
(** [write_byte ram addr byte] writes the boolean array [byte] of length 8 at
    address [addr]. *)

val write_halfword : t -> int -> bool array -> unit
(** Same as {!write_byte} but writes an halfword (length 16) at addresses [addr]
    and [addr+1]. The address must be even. *)

val write_word : t -> int -> bool array -> unit
(** Same as {!write_byte} but writes a word (length 32) at addresses [addr]
    to [addr+4]. The address must be a multiple of 4. *)


(** {2 Printing} *)

val print_ram : out_channel -> ?range:int*int -> t -> unit
(** Displays the content of the RAM between addresses specified in [range]. If
    not specified, the full RAM is displayed. *)
