let failarg str = raise @@ Invalid_argument str


let int_of_bool = function
  | true -> 1
  | false -> 0

let bool_of_int = function
  | 0 -> false
  | 1 -> true
  | n -> failarg @@ Printf.sprintf "bool_of_int: %d (undefined)" n


let bool_of_char = function
  | '0' -> false
  | '1' -> true
  | _ -> failarg "Interpreter.parse_bit"


let barray_of_string s = 
  Array.init (String.length s) (fun i -> bool_of_char s.[i])
