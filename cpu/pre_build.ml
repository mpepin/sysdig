(*
  Computes all the .mj sources in one single file.
  The dependencies of a file must be specified at its beginning with one
  statement [require dep] per line for each dependency [dep]. There must be no
  empty lines between the [require] statements.
*)


(* Options *)
let ofile = ref "a.mj"
let src_dir = ref ""
let set_str = (:=)

(* Modules handling *)
type mj_module = Main of string | Module of string
let get_name = function Main name | Module name -> name


(**
 * Core functions
 **)

let regexp = Str.regexp "^require \\([/a-zA-Z0-9_]+\\) *$"
let open_src src =
  Printf.sprintf "%s%s%s.mj" !src_dir Filename.dir_sep src
  |> open_in

let rec copy ic oc =
  try
    input_line ic |> Printf.fprintf oc "%s\n";
    copy ic oc
  with End_of_file -> close_in ic ; flush oc


let rec fetch_deps ic = 
  let line = input_line ic in
  try
    if Str.string_match regexp line 0 then
      let next, deps = fetch_deps ic in
      (next, (Module (Str.replace_first regexp "\\1" line))::deps)
    else line, []
  with End_of_file -> "", []


let rec generate imported oc mj_mod =
  let name = get_name mj_mod in
  if not @@ Hashtbl.mem imported name then begin
    let ic = match mj_mod with
      | Main name -> open_in name
      | Module name -> open_src name
    in

    (* Fetching dependencies *)
    let next, deps = fetch_deps ic in
    (* Computing dependencies *)
    List.iter (generate imported oc) deps;
    (* Computing files *)
    Printf.fprintf oc "\n(* %s.mj *)\n\n" name ;
    Printf.fprintf oc "%s\n" next;
    copy ic oc;

    close_in ic;
    match mj_mod with
      | Main _ -> ()
      | Module name ->
          Printf.printf "[Imported] %s.mj\n" name;
    Hashtbl.add imported name ()
  end


let build main_file =
  let oc = open_out !ofile in
  let imported = Hashtbl.create 17 in
  if not (Filename.check_suffix main_file ".mj") then begin
    Format.eprintf
      "Wrong input file %s. It should have the .mj extension.@."
      main_file;
    exit 1
  end;
  generate imported oc @@ Main main_file;
  close_out oc;
  Printf.printf "[Complete] %s\n" !ofile


(**
 * Main
 **)

let () =
  Arg.parse
    ["-o", Arg.String (set_str ofile),
        "Output file, the default is a.mj";
     "--sources", Arg.String (set_str src_dir),
        "A unique folder in which the script will look for modules. The \
         default is the current folder."]
    build
    "usage: pre_build [--sources <src>] [-o <output>] <main file>"
